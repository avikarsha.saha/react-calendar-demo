import React, { Component } from 'react';
import DateRangePicker from 'react-daterange-picker';
import moment from 'moment';
import 'react-daterange-picker/dist/css/react-calendar.css';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dates: null,
      entries: []
    };
  }
dateRangeOverlaps = (a, b) => {
    /*console.log("a_start",a.start);
    console.log("a_end",a.end_date);
    console.log("b_start",b.start);
    console.log("b_end_date",b.end_date);*/
    if (a.start_date <= b.start_date && b.start_date <= a.end_date) return true; // b starts in a
    if (a.start_date <= b.end_date  && b.end_date   <= a.end_date) return true; // b end_dates in a
    if (b.start_date <  a.start_date && a.end_date   <  b.end_date) return true; // a in b
    return false;
}
multipleDateRangeOverlaps = (key) => {
  console.log("data",this.state.entries);
  console.log("item", key);
    var i;
    for (i = 0; i < this.state.entries.length; i += 1) {
      if (key != i && this.dateRangeOverlaps(this.state.entries[i], this.state.entries[key])) 
        return true;
    }
    return false;
}
  onSelect = (dates) => {

   // console.log("dates",dates.start.format('YYYY-MM-DD'),dates.end.format('YYYY-MM_DD'));
    this.setState({dates: dates});
    var data = {
      "start_date": dates.start.format('YYYY-MM-DD'),
      "end_date": dates.end.format('YYYY-MM-DD')
    }

    /*let query = Object.keys(data)
      .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(data[k]))
      .join('&');

    fetch("http://localhost:5000/eventget?"+query, {
        method: "GET",
    })
    .then((response)=>{ 
        return response.json(); 
    })
    .then((data)=>{ 
        console.log(data);
        this.setState({
          entries: data
        })
    });*/

    var headers = {
      "Content-Type": "application/json;charset=UTF-8",  
      "Accept": "application/json, text/plain, */*",                                                                                              
      "Access-Control-Origin": "*"
    }
    fetch("http://112.196.104.171:3001/api/GetEvents", {
        method: "POST",
        mode: "cors",
        headers: headers,
        body:  JSON.stringify(data)
    })
    .then(function(response){ 
        return response.json(); 
    })
    .then((data)=>{ 
        console.log(data);
        this.setState({
          entries: data.result
        })
    });

  }
  render() {
    console.log("dates",this.state.dates);
    return (
      <div className="App">
          <h1>Calendar demo app</h1>
          <DateRangePicker
          onSelect={this.onSelect}
          value={this.state.dates}
        />
        <ul>
          {
              this.state.entries.map((item, key) => {
                  console.log(item);
                  var status = this.multipleDateRangeOverlaps(key);
                  console.log(status);
                  return (
                    <li key={key} style={status ? {'color': 'red'} : {}}
                    >Start date: {moment(item.start_date).format("YYYY-MM-DD")} - End date: {moment(item.end_date).format("YYYY-MM-DD")} - Title: {item.title}</li>
                  )
              })
          }
        </ul>
      </div>    );
  }
}

export default App;